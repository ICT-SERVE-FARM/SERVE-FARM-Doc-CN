Usage (local)
=============

.. Build a local VIVADO environment:

Build a local VIVADO environment
--------------------------------

To use SERVE-FARM, first install VIVADO:

(We recommend that you install the VIVADO-2019.1 version, which has been fully tested. If you want to use a newer version, please pay attention to some details, such as you may need to upgrade the version of some IP cores, modify some configuration items, etc.)

Ref:
https://china.xilinx.com/products/design-tools/vivado.html
https://www.xilinx.com/support/download.html

.. code-block:: console

    $ ./xsetup


Clone the repository
--------------------

For example:

.. code-block:: console

   $ git clone https://gitlab.agileserve.org.cn:8001/ICT-SERVE-FARM/SERVE-FARM.git
   $ cd SERVE-FARM

   #Run this command to update subrepositories used by SERVE-FARM
   $ git submodule update --init --recursive


Platform setup
--------------



RISC-V Processor/SoC Generation
-------------------------------



Compilation flags of ZSBL
-------------------------



Generation of FPGA Prototyping
------------------------------



Software design
---------------



