欢迎使用 SERVE-FARM!
===================================

**欢迎使用 SERVE-FARM! (中文版说明)**

本仓库致力于创建一个完整的软硬件集成环境，面向RISC-V相关设计。由中国科学院计算技术研究所SERVE团队开发维护。

基于SERVE-FARM存储库，您可以轻松将开源RISC-V内核（目前支持 `rocket-chip <https://github.com/chipsalliance/rocket-chip.git>`_ 与 `NutShell <https://github.com/OSCPU/NutShell.git>`_ 两种内核）部署到FPGA平台中。在这个仓库中，我们不仅提供了RISC-V启动和运行所需的硬件环境，包括时钟、复位、内存访问接口、串口、网络等必要的外设；我们还提供从开机到运行操作系统所需的所有软件的 RISC-V 内核，包括 ZSBL、OpenSBI、Uboot 和 Linux 内核。查看简介部分了解更多信息。

目前可以将SERVE-FARM仓库克隆到本地电脑，通过一系列make指令搭建RISC-V内核所需的软硬件环境，将make生成的文件拷贝到SD卡启动RISC-V 核心 [ref: :doc:`usage(local)`]。同时，我们也提供基于 CI/CD 的方法 [ref: :doc:`usage(CICD)`]。




.. 注意::

   

章节内容
--------

.. toctree::
   :numbered:
   
   intro 简介
   usage(local) 本地用法
   usage(CICD) CI用法
